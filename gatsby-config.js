/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

module.exports = {
  siteMetadata: {
    title: `Erick Surya Dinata | Back-end Developer`,
    author: `Erick Surya Dinata`,
    description: `"Geek" teknologi dengan skill dari berbagai bidang IT. Membangun produk dari awal, melihat hal dari berbagai sisi, dan membuatnya menjadi sesuatu yang spesial. Dengan skill dan pengalaman yang saya miliki selama 3 tahun, saya membantu perusahaan dari ide produk hingga digunakan. Saya mendesain dan membangun solusi yang dapat dikembangkan lebih lanjut, mengimplentasikan proses yang cocok dengan perusahaan, membangun tim yang menghasilkan produk`,
    siteUrl: `https://ericksuryadinata.gitlab.io/`,
    social: {
      gitlab: `ericksuryadinata`,
    }
  },
  plugins: [
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Erick Surya Dinata | Back-end Developer`,
        short_name: `ESD`,
        start_url: `/`,
        background_color: `#F7F0EB`,
        theme_color: `#2732D6`,
        display: `standalone`,
        icon: 'src/assets/site-icon.png'
      },
    },
    'gatsby-plugin-offline',
    'gatsby-transformer-json',
    {
      resolve: `gatsby-source-filesystem`,
      options: {
        name: `data`,
        path: `${__dirname}/data/`,
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /assets/,
        },
      },
    },
    'gatsby-plugin-netlify-cms',
    `gatsby-plugin-postcss`,
    {
      resolve: `gatsby-plugin-purgecss`,
      options: {
        printRejected: true,
        tailwind: true, // Enable tailwindcss support
      }
    }
  ],
};
